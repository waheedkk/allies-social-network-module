
var mongodb = require( __dirname + '/db' );

// create a schema
var FacebookShareSchema = mongodb.Schema({
  websites_id: { type: mongodb.Schema.Types.ObjectId, ref: 'Website'},
  user_id: { type: mongodb.Schema.Types.ObjectId, ref: 'User' },
  post : String, // eg some text + websiteLink
  created_at: Date,
  updated_at: Date
});

// the schema is useless so far
// we need to create a model using it
var FacebookShare = mongodb.model('FacebookShare', FacebookShareSchema);

// make this available to our users in our Node applications
module.exports = FacebookShare;
var mongodb = require( __dirname + '/db' );

// create a schema
var UserSchema = mongodb.Schema({
  name: String,
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  admin: Boolean,
  location: String,
  websites: [ 
  	{ type: mongodb.Schema.Types.ObjectId, ref: 'Website'}
  ],
  servicesUsed: [
    { type: mongodb.Schema.Types.ObjectId, ref: 'FacebookShare' }  // this will work for FacebookShare service in future we will create general service class whcih contain all service data
  ],
  created_at: Date,
  updated_at: Date
});

// the schema is useless so far
// we need to create a model using it
var User = mongodb.model('User',UserSchema);

// make this available to our users in our Node applications
module.exports = User;
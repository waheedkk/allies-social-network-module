var mongodb = require( __dirname + '/db' );

// create a schema
var WebsiteSchema = mongodb.Schema({
  link: String,
  type: String,  // eg: entertainment
  servicesRequired : [
    facebookShares: Boolean,
    facebookLikes: Boolean,
    googlePlus: Boolean,
    pinterestPins: Boolean, 
    twitterTweets: Boolean, 
    tumblrPosts: Boolean, 
    stumbleuponPosts: Boolean, 
    wordPressPosts: Boolean,
    bloggerPosts: Boolean,
    clicksWebTraffic: Boolean, 
    autosurfWebTraffic: Boolean
  ],
  watchedBy: [ 
    { type: mongodb.Schema.Types.ObjectId, ref: 'User'}
  ],
  clickedBy: [ 
    { type: mongodb.Schema.Types.ObjectId, ref: 'User'}
  ],
  servicesUsed: [
    
  ],
  created_at: Date,
  updated_at: Date
});

// the schema is useless so far
// we need to create a model using it
var Website = mongodb.model('Website', WebsiteSchema);

// make this available to our users in our Node applications
module.exports = Website;